#!/bin/bash

name=openfoam
gitCommit="a0e4ecb4d630bc2"
version=1.6-ext-$gitCommit


if test ! -d OpenFOAM-1.6-ext 
then
	git clone git://git.code.sf.net/p/openfoam-extend/OpenFOAM-1.6-ext 
fi

#module load compilers/gcc/4.8.0
#module load apps/blcr/0.8.4
#module load mpi/openmpi/1.6.4_gcc

module load compilers/gcc/4.4.2
module load mpi/openmpi/1.4.3_gcc 

rm -rf mock
cp -r OpenFOAM-1.6-ext mock
cd mock
git checkout $gitCommit

#root=/
root=~/fake-root-gcc-4-4-2

prefix=$root/software/apps/$name/$version
mkdir -p $prefix
export FOAM_INST_DIR=$prefix

myFoamPath=$prefix/OpenFOAM-1.6-ext
cp -r . $myFoamPath

cd $myFoamPath

cp etc/prefs.sh-EXAMPLE etc/prefs.sh

source $myFoamPath/etc/bashrc

./Allwmake &> build.log
