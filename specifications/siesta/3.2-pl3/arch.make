#
# Based on:
#
# Link: http://www.hpckp.org/index.php/articles-tutorials/148-howto-siesta-30rc2-wib
#
#
# This file is part of the SIESTA package.
#
# Copyright (c) Fundacion General Universidad Autonoma de Madrid:
# E.Artacho, J.Gale, A.Garcia, J.Junquera, P.Ordejon, D.Sanchez-Portal
# and J.M.Soler, 1996- .
# 
# Use of this software constitutes agreement with the full conditions
# given in the SIESTA license, as signed by all legitimate users.
#
SIESTA_ARCH=intel-mkl
#
# Intel fortran compiler for linux with mkl optimized blas and lapack
#
# Be sure to experiment with different optimization options.
# You have quite a number of combinations to try...
#
FC=mpif90 
#
FFLAGS= -w -mp -O2  -xSSE4.2 -static-intel 
FFLAGS_DEBUG= -g
FFLAGS_CHECKS= -g -O0 -debug full -traceback -C  
LDFLAGS=-Vaxlib 
COMP_LIBS=
RANLIB=ranlib
#
NETCDF_LIBS=
NETCDF_INTERFACE=
FPPFLAGS_CDF=

#NETCDF_LIBS=  /software/misc-libs/netcdf-4.1.1_intel/libnetcdf.a
#NETCDF_INTERFACE= libnetcdff.a
#FPPFLAGS_CDF= -DCDF

#
MPI_INTERFACE=libmpi_f90.a
#MPI_INCLUDE=/aplic/MPI/OpenMPI/1.4.2_intel-10.1_ofed-1.5.1_blcr-8.2/include/
MPI_INCLUDE=/software/mpi/openmpi/1.6.4_intel/include/
FPPFLAGS_MPI=-DMPI -DFC_HAVE_FLUSH -DFC_HAVE_ABORT
DEFS_MPI= -DMPI
#
#MKLPATH=/opt/intel/mkl/10.1.3.027/lib/em64t
MKLPATH=/software/intel/composer_xe_2013/mkl
MKLROOT=$(MKLPATH)
# $(MKLPATH)/libmkl_solver_lp64.a
#LIBS= -L$(MKLPATH) -lmkl $(MKLPATH)/libmkl_scalapack_lp64.a  -Wl,--start-group $(MKLPATH)/libmkl_intel_lp64.a $(MKLPATH)/libmkl_intel_thread.a $(MKLPATH)/libmkl_core.a $(MKLPATH)/libmkl_blacs_openmpi_lp64.a -Wl,--end-group -openmp -lpthread
LIBS= $(MKLROOT)/lib/intel64/libmkl_blas95_lp64.a $(MKLROOT)/lib/intel64/libmkl_lapack95_lp64.a $(MKLROOT)/lib/intel64/libmkl_scalapack_lp64.a -Wl,--start-group  $(MKLROOT)/lib/intel64/libmkl_intel_lp64.a $(MKLROOT)/lib/intel64/libmkl_sequential.a $(MKLROOT)/lib/intel64/libmkl_core.a $(MKLROOT)/lib/intel64/libmkl_blacs_openmpi_lp64.a -Wl,--end-group -lpthread -lm

SYS=bsd
FPPFLAGS= $(FPPFLAGS_CDF) $(FPPFLAGS_MPI)
#
.F.o:
	$(FC) -c $(FFLAGS) $(INCFLAGS)  $(FPPFLAGS) $<
.f.o:
	$(FC) -c $(FFLAGS) $(INCFLAGS)   $<
.F90.o:
	$(FC) -c $(FFLAGS) $(INCFLAGS)  $(FPPFLAGS) $<
.f90.o:
	$(FC) -c $(FFLAGS) $(INCFLAGS)   $<
#
