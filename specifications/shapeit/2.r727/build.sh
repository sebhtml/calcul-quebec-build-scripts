#!/bin/bash
# Author: Sébastien Boisvert

name=shapeit
version=2.r727
release=1

source=http://www.shapeit.fr/files/shapeit.v2.r727.linux.x64.tar.gz

tarball=$(basename $source)

if test ! -f $tarball
then
	wget $source
fi

rm -rf fake-root

#where=$(pwd)/fake-root
where=/

installHome=$where/software/apps/$name
moduleHome=$where/clumeq/Modules/modulefiles/apps/$name

prefix=$installHome/$version-$release

snapshot=$(pwd)

rm -rf mock

mkdir mock
cd mock
tar -xzf ../$tarball

#
#cd $(ls|head -n1)

##################
# specific commands are below

# copy stuff in $prefix.

mkdir -p $prefix/bin
cp shapeit.v2.r727.linux.x64 $prefix/bin
ln -s shapeit.v2.r727.linux.x64 shapeit
cp shapeit $prefix/bin

rm -rf example/.svn

mkdir -p $prefix/usr/share/shapeit
cp -r example $prefix/usr/share/shapeit

# end of specific commands
################

cd $snapshot

mkdir -p $moduleHome
cp modulefile.txt $moduleHome/$version-$release

chgrp clumeq -R $installHome
chgrp clumeq -R $moduleHome
chmod g+w -R $installHome
chmod g+w -R $moduleHome
