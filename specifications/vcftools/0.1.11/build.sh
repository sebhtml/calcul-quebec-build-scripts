#!/bin/bash
# Author: Sébastien Boisvert

name=vcftools
version=0.1.11
release=2

source=http://softlayer-dal.dl.sourceforge.net/project/vcftools/vcftools_0.1.11.tar.gz

tarball=$(basename $source)

if test ! -f $tarball
then
	wget $source
fi

rm -rf fake-root

#where=$(pwd)/fake-root
where=/

installHome=$where/software/apps/$name
moduleHome=$where/clumeq/Modules/modulefiles/apps/$name

prefix=$installHome/$version-$release

snapshot=$(pwd)

rm -rf mock

mkdir mock
cd mock
tar -xzf ../$tarball

#
cd $(ls|head -n1)

##################
# specific commands are below

mkdir -p $prefix/bin

# copy stuff in $prefix.

module load compilers/gcc/4.8.0

make

PREFIX=$prefix make install

# the Makefile is not copying this...

cp bin/vcftools $prefix/bin

mkdir $prefix/usr/share/vcftools -p

cp -r examples $prefix/usr/share/vcftools

# end of specific commands
################

cd $snapshot

mkdir -p $moduleHome
cp modulefile.txt $moduleHome/$version-$release

chgrp clumeq -R $installHome
chgrp clumeq -R $moduleHome
chmod g+w -R $installHome
chmod g+w -R $moduleHome
