#!/bin/bash

root=/software/centos-release-5-8.el5.centos.x86_64

rpm=$1
force=""

if test "$2" = "--force"
then
	force="--nodeps"
fi

# rpm --initdb --root $root 

rpm --root $root --relocate /=$root/ -ivh $force $rpm
