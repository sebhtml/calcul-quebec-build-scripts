#!/bin/bash
# Author: Sébastien Boisvert
#

name=alugrid
version=1.52
release=2

source=http://aam.mathematik.uni-freiburg.de/IAM/Research/alugrid/ALUGrid-1.52.tar.gz

tarball=$(basename $source)

if test ! -f $tarball
then
	wget $source
fi

rm -rf $name-fake-root

where=$(pwd)/fake-root
#where=/

installHome=$where/software/apps/$name
moduleHome=$where/clumeq/Modules/modulefiles/apps/$name

prefix=$installHome/$version-$release

snapshot=$(pwd)

rm -rf mock

mkdir mock
cd mock
tar -xzf ../$tarball

cd $(ls|head -n1)

########################################

module load compilers/gcc/4.7.2
module load mpi/openmpi/1.6.3_gcc


./configure CXXFLAGS="-DNDEBUG -g -O2" CXX=mpiCC --with-metis=/rap/jda-332-aa/misc-libs/metis/5.1.0-1/ \
--prefix=$prefix

make
make install

##############################

cd $snapshot

mkdir -p $moduleHome
#cp modulefile.txt $moduleHome/$version-$release

chgrp clumeq -R $installHome
chgrp clumeq -R $moduleHome
chmod g+w -R $installHome
chmod g+w -R $moduleHome
