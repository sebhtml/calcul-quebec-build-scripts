#!/bin/bash

yum reinstall $1  -y --downloadonly --downloaddir=$(pwd)/RPMS |tee log

if test $(grep "not installed" log|wc -l) -eq 1
then
	yum install $1  -y --downloadonly --downloaddir=$(pwd)/RPMS &> log
fi


ls $(pwd)/RPMS | grep $1

