#!/bin/bash
# Author: Sébastien Boisvert

name=openmpi
version=1.6.5
release=1

source0=http://www.open-mpi.org/software/ompi/v1.6/downloads/openmpi-1.6.5.tar.gz

module load compilers/gcc/4.8.0

###################################

tarball=$(basename $source0)

if test ! -f $tarball
then
	wget $source0   -O $tarball
fi


rm -rf fake-root

where=$(pwd)/fake-root
#where=/

installHome=$where/rap/jda-332-aa/apps/$name
moduleHome=$where/rap/jda-332-aa/modulefiles/apps/$name

prefix=$installHome/$version-$release

snapshot=$(pwd)

rm -rf mock

mkdir mock
cd mock
tar -xzf ../$tarball

mkdir build
cd build

##################
# specific commands are below


export CFLAGS='-O2 -march=native -pipe -Wall'

CFLAGS="$CFLAGS" \
CPPFLAGS=-I/usr/include/torque \
../$name-$version/configure --prefix=$prefix \
--with-threads --enable-mpi-thread-multiple --with-openib \
--enable-shared --enable-static --with-ft=cr --enable-ft-thread \
--with-blcr=/software/apps/blcr/0.8.4 \
--with-blcr-libdir=/software/apps/blcr/0.8.4/lib \
--with-io-romio-flags="--with-file-system=testfs+ufs+nfs+lustre" \

make -j 4

make install


# end of specific commands
################

cd $snapshot

mkdir -p $moduleHome
cp modulefile.txt $moduleHome/$version-$release

chgrp clumeq -R $installHome
chgrp clumeq -R $moduleHome
chmod g+w -R $installHome
chmod g+w -R $moduleHome
