#!/bin/bash
# Author: Sébastien Boisvert

name=chromocombine
version=0.0.4
release=1

source=http://www.maths.bris.ac.uk/%7Emadjl/finestructure/chromocombine-0.0.4.tar.gz

tarball=$(basename $source)

if test ! -f $tarball
then
	wget $source
fi

rm -rf $name-fake-root
where=$(pwd)/$name-fake-root
where=""

installHome=$where/rap/tem-670-aa/apps/$name
moduleHome=$where/rap/tem-670-aa/modulefiles/apps/$name

prefix=$installHome/$version-$release

snapshot=$(pwd)

rm -rf mock

mkdir mock
cd mock
tar -xzf ../$tarball

module load compilers/gcc/4.8.0
module load misc-libs/gsl/1.15

cd $(ls|head -n1)
./configure --prefix=$prefix 

make -j 4

make install

cd $snapshot

mkdir -p $moduleHome
cp modulefile.txt $moduleHome/$version-$release

chgrp clumeq -R $installHome
chgrp clumeq -R $moduleHome
chmod g+w -R $installHome
chmod g+w -R $moduleHome
