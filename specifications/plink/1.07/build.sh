#!/bin/bash

name=plink
version=1.07
release=1

source=http://pngu.mgh.harvard.edu/~purcell/plink/dist/plink-1.07-src.zip

tarball=$(basename $source)

if test ! -f $tarball
then
	wget $source
fi

rm -rf fake-root
mkdir fake-root

module load compilers/gcc/4.8.0

where=$(pwd)/fake-root
where=/

softwarePrefix=$where/software
modulePrefix=$where/clumeq/Modules/modulefiles

installHome=$softwarePrefix/apps/$name
moduleHome=$modulePrefix/apps/$name

prefix=$installHome/$version-$release

snapshot=$(pwd)

rm -rf mock

mkdir mock
cd mock
unzip ../$tarball

cd $(ls|head -n1)

##############################################
# build the product

patch -p2 < $snapshot/iterator-patch.patch

make -j 4
mkdir -p $prefix/bin
cp plink $prefix/bin
mkdir -p $prefix/documentation
cp COPYING.txt README.txt $prefix/documentation
mkdir -p $prefix/lib
cp gPLINK.jar $prefix/lib
mkdir -p $prefix/examples
cp test.map test.ped  $prefix/examples


##############################
cd $snapshot

mkdir -p $moduleHome
cp modulefile.txt $moduleHome/$version-$release

chgrp clumeq -R $installHome
chgrp clumeq -R $moduleHome
chmod g+w -R $installHome
chmod g+w -R $moduleHome
