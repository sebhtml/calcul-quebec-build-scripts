#!/bin/bash

source=http://creskolab.uoregon.edu/stacks/source/stacks-1.03.tar.gz
archive=$(basename $source)

if test ! -f $archive
then
	wget $source
fi

cp modulefile /rap/tem-670-aa/modulefiles/apps/stacks/1.03-1

rm -rf box
mkdir box
cd box
cat ../$archive|gunzip|tar -x

module load compilers/gcc/4.8.0
cd $(ls)
prefix=/rap/tem-670-aa/apps/stacks/1.03-1
./configure --prefix=$prefix
make
make install
chgrp clumeq -R $prefix
chmod g+w -R $prefix

chgrp clumeq -R /rap/tem-670-aa/modulefiles/apps/stacks/1.03-1
chmod g+w /rap/tem-670-aa/modulefiles/apps/stacks/1.03-1
