#!/bin/bash
# Author: Sébastien Boisvert

name=dumux
version=2.4.0
release=2

source0=http://www.dumux.uni-stuttgart.de/download/$name-$version.tar.gz

##########################################

# same modules that we used for DuMuX 2.3.0
module load compilers/gcc/4.7.2
module load misc-libs/gmp/5.0.2_gcc
module load mpi/openmpi/1.6.3_gcc

###################################

tarball=$(basename $source0)

if test ! -f $tarball
then
	wget $source0  --user=dumux --password=download  -O $tarball
fi

count=0
otherFiles[$count]=http://www.dune-project.org/download/2.2.1/dune-common-2.2.1.tar.gz
count=$(($count + 1))
otherFiles[$count]=http://www.dune-project.org/download/2.2.1/dune-grid-2.2.1.tar.gz
count=$(($count + 1))
otherFiles[$count]=http://www.dune-project.org/download/2.2.1/dune-geometry-2.2.1.tar.gz
count=$(($count + 1))
otherFiles[$count]=http://www.dune-project.org/download/2.2.1/dune-istl-2.2.1.tar.gz
count=$(($count + 1))
otherFiles[$count]=http://www.dune-project.org/download/2.2.1/dune-localfunctions-2.2.1.tar.gz
count=$(($count + 1))
otherFiles[$count]=http://www.dune-project.org/download/2.2.1/dune-grid-howto-2.2.1.tar.gz
count=$(($count + 1))
otherFiles[$count]=http://www.dune-project.org/download/2.2.1/dune-grid-dev-howto-2.2.1.tar.gz
count=$(($count + 1))
otherFiles[$count]=http://www.dune-project.org/download/pdelab/1.1/dune-pdelab-1.1.0.tar.gz
#count=$(($count + 1))


for i in $(seq 0 $count)
do
	address=${otherFiles[$i]}

	localFile=$(basename $address)

	if test ! -f $localFile
	then
		wget $address -O $localFile
	fi
done

rm -rf fake-root

where=$(pwd)/fake-root
#where=/

installHome=$where/rap/jda-332-aa/apps/dumux/
moduleHome=$where/rap/jda-332-aa/modulefiles/apps/$name

prefix=$installHome/$version-$release

snapshot=$(pwd)

rm -rf mock

path=/rap/jda-332-aa/apps/dumux-2.4.0-2

mkdir $path
cd $path

tar -xzf $snapshot/$tarball

##################
# specific commands are below

for i in $(seq 0 $count)
do
	address=${otherFiles[$i]}

	localFile=$snapshot/$(basename $address)

	tar -xzf $localFile
done

cd dumux-*
cp $snapshot/optim.opts .
export DUMUX_ROOT=$(pwd)
cd ..


# apply patch for PDE
cd $(ls|grep pde)
patch -p0 < ../dumux-2.4.0/patches/pdelab-1.1.0.patch
cd ..

# apply patch for istl
cd $(ls |grep istl)
patch -p0 <../dumux-2.4.0/patches/istl-2.2.0.patch
cd ..

# call dunecontrol
./dune-common-2.2.1/bin/dunecontrol --opts=$DUMUX_ROOT/optim.opts --module=dumux all 2>&1 | tee log.txt


# end of specific commands
################

cd $snapshot

mkdir -p $moduleHome
#cp modulefile.txt $moduleHome/$version-$release

chgrp clumeq -R $installHome
chgrp clumeq -R $moduleHome
chmod g+w -R $installHome
chmod g+w -R $moduleHome

chmod o-rwx -R $path
chmod g+rwx -R $path
chgrp clumeq -R $path
setfacl -R -d -m g:jda-332-01:rwx $path
setfacl -R  -m g:jda-332-01:rwx $path
