#!/bin/bash
# Author: Sébastien Boisvert

name=fastphase
version=1.4.0
release=1

source=http://depts.washington.edu/fphase/download/fastPHASE_Linux.gz

tarball=$(basename $source)

if test ! -f $tarball
then
	fetch="wget --user=fastPHASEuser --password=24minutes"

	$fetch $source
	$fetch http://depts.washington.edu/fphase/download/fastphase_doc_1.2.pdf
	$fetch http://depts.washington.edu/fphase/download/fastphase.inp
	$fetch http://depts.washington.edu/fphase/download/fastphase_haplotypes.inp
	$fetch http://depts.washington.edu/fphase/download/fastphase_subpoplabels.inp
	$fetch http://depts.washington.edu/fphase/download/fastphase_subpoplabels2.inp
fi

rm -rf $name-fake-root
where=$(pwd)/$name-fake-root
where=""

rm -rf mock
mkdir mock

snapshot=$(pwd)

cd mock

product=fastPHASE_Linux
cat ../$tarball | gunzip > $product
chmod +x $product


installHome=$where/rap/tem-670-aa/apps/$name
moduleHome=$where/rap/tem-670-aa/modulefiles/apps/$name

prefix=$installHome/$version-$release

mkdir -p $prefix/bin
cp $product $prefix/bin

mkdir -p $moduleHome

cd $snapshot
cp modulefile.txt $moduleHome/$version-$release
mkdir $prefix/documentation
cp *.pdf *.inp $prefix/documentation

chgrp clumeq -R $installHome
chmod g+w -R $installHome
chgrp clumeq -R $moduleHome
chmod g+w -R $moduleHome
