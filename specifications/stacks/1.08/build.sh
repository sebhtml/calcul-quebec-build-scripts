#!/bin/bash
# Author: Sébastien Boisvert

name=stacks
version=1.08
release=1

source=http://creskolab.uoregon.edu/$name/source/$name-$version.tar.gz
archive=$(basename $source)

if test ! -f $archive
then
	wget $source
fi

#root=$(pwd)/fake-root
root=/

moduleRoot=$root/rap/tem-670-aa/modulefiles/apps/$name/
prefix=$root/rap/tem-670-aa/apps/$name/$version-$release

mkdir -p $moduleRoot
cp modulefile $moduleRoot/$version-$release

rm -rf box
mkdir box
cd box
cat ../$archive|gunzip|tar -x

module load compilers/gcc/4.8.0

cd $(ls)
./configure --prefix=$prefix
make
make install
chgrp clumeq -R $prefix
chmod g+w -R $prefix

chgrp clumeq -R $moduleRoot
chmod g+w $moduleRoot
