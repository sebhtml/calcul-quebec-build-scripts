#!/bin/bash
# Author: Sébastien Boisvert
#
# Link: http://www.hpckp.org/index.php/articles-tutorials/148-howto-siesta-30rc2-wib
#
# Link: http://inside.mines.edu/~nbrawand/siestaparallel2.txt
#
# For MKL options:
# Link: http://software.intel.com/sites/products/mkl/MKL_Link_Line_Advisor.html

name=siesta
version=3.2-pl3
release=3

# based on the documentation
# siesta-3.2-pl3/Docs/siesta.tex

source=http://icmab.cat/leem/siesta/CodeAccess/Code/siesta-3.2-pl3.tgz

tarball=$(basename $source)

if test ! -f $tarball
then
	wget $source
fi

rm -rf fake-root
mkdir fake-root

where=$(pwd)/fake-root
where=/

softwarePrefix=$where/software
modulePrefix=$where/clumeq/Modules/modulefiles

installHome=$softwarePrefix/apps/$name
moduleHome=$modulePrefix/apps/$name

prefix=$installHome/$version-$release

snapshot=$(pwd)

rm -rf mock

mkdir mock
cd mock
tar -xzf ../$tarball

cd $(ls|head -n1)

cd Obj
sh ../Src/obj_setup.sh

#cp $snapshot/arch.make .

# TODO: add these dependencies:
  #--with-PACKAGE[=ARG]    use PACKAGE [ARG=yes]
  #--without-PACKAGE       do not use PACKAGE (same as --with-PACKAGE=no)
  #--with-netcdf=<lib>     use NetCDF library <lib>
  #--with-siesta-blas      use BLAS library packaged with SIESTA
  #--with-blas=<lib>       use BLAS library <lib>
  #--with-siesta-lapack    use LAPACK library packaged with SIESTA
  #--with-lapack=<lib>     use LAPACK library <lib>
  #--with-blacs=<lib>      use BLACS library <lib>
  #--with-scalapack=<lib>  use ScaLAPACK library <lib>

module load compilers/intel/2013 # compiler
#module load misc-libs/netcdf/4.3.0_gcc  # netcdf
module load misc-libs/netcdf/4.1.1_intel
module load blas-libs/mkl/11.0 # blas and lapack
module load mpi/openmpi/1.6.4_intel # mpi

# I did not find these on colosse:
#--with-scalapack \
#--with-blacs \

#######
#--with-netcdf=/software/misc-libs/netcdf-4.1.1_intel \
#--with-blas \
#--with-lapack \
#--without-blacs \

#--with-blacs=/software/intel/composer_xe_2013/mkl/lib/intel64/libmkl_blacs_openmpi_lp64.a \
#--with-lapack=/software/intel/composer_xe_2013/mkl/lib/intel64/libmkl_lapack95_lp64.a \
#--with-blas=/software/intel/composer_xe_2013/mkl/lib/intel64/libmkl_blas95_lp64.a \
#--with-scalapack=/software/intel/composer_xe_2013/mkl/lib/intel64/libmkl_scalapack_lp64.a \



#../Src/configure \
#FC=mpif90 \
#--enable-mpi \
#--with-blacs \
#--with-lapack \
#--with-blas \
#--with-scalapack \
#--without-siesta-blas \
#--without-siesta-lapack \
#FCFLAGS=" -I$MKLROOT/include/intel64/lp64 -I$MKLROOT/include -L " \
#LDFLAGS=" $MKLROOT/lib/intel64/libmkl_blas95_lp64.a $MKLROOT/lib/intel64/libmkl_lapack95_lp64.a $MKLROOT/lib/intel64/libmkl_scalapack_lp64.a -Wl,--start-group  $MKLROOT/lib/intel64/libmkl_intel_lp64.a $MKLROOT/lib/intel64/libmkl_sequential.a $MKLROOT/lib/intel64/libmkl_core.a $MKLROOT/lib/intel64/libmkl_blacs_openmpi_lp64.a -Wl,--end-group -lpthread -lm" \
#2>&1 | tee $snapshot/configure.log 

################################
# build siesta
#make install

cp $snapshot/arch.make .
make clean
make -j 1 \
2>&1 | tee $snapshot/make.log

mkdir -p $prefix/bin
mkdir -p $prefix/documentation
cp siesta $prefix/bin

################################
######## Now, build tbtrans without MPI support

cp $snapshot/arch.make .

# patch the arch file to remove MPI support.

sed 's/DEFS_MPI= -DMPI/DEFS_MPI=/g' -i arch.make
cp arch.make tbtrans-arch.make
make clean
make TBTrans
cd ../Util/TBTrans/
make
cp tbtrans $prefix/bin
cd ../../Obj


################################
# build transiesta
cp $snapshot/arch.make .
make clean
# make transiesta
make transiesta -j 1 \
2&>1 | tee $snapshot/make-transiesta.log
cp transiesta $prefix/bin

cp ../Docs/siesta.tex $prefix/documentation

mkdir -p $prefix/usr/share/siesta
cp -r $snapshot/mock/siesta-3.2-pl3/Tests $prefix/usr/share/siesta
cp -r $snapshot/mock/siesta-3.2-pl3/Examples $prefix/usr/share/siesta

cd $snapshot

mkdir -p $moduleHome
cp modulefile.txt $moduleHome/$version-$release

chgrp clumeq -R $installHome
chgrp clumeq -R $moduleHome
chmod g+w -R $installHome
chmod g+w -R $moduleHome
